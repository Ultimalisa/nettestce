#include "StdAfx.h"
#include "Player.h"
#include "Bullet.h"
#include "SpawnPoint.h"
#include "GamePlugin.h"

#include <CryNetwork/Rmi.h>
#include <CryPhysics/IPhysics.h>
#include "ServerComponent.h"

namespace
{
	static void RegisterPlayerComponent(Schematyc::IEnvRegistrar& registrar)
	{
		Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
		{
			Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CPlayerComponent));
		}
	}

	CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterPlayerComponent);
}

void CPlayerComponent::Initialize()
{
	CGamePlugin::GetInstance()->m_pServer->m_pPlayerV.push_back(this);

	// The character controller is responsible for maintaining player physics
	m_pCharacterController = m_pEntity->GetOrCreateComponent<Cry::DefaultComponents::CCharacterControllerComponent>();
	// Offset the default character controller up by one unit
	m_pCharacterController->SetTransformMatrix(Matrix34::Create(Vec3(1.f), IDENTITY, Vec3(0, 0, 1.f)));

	// Create the advanced animation component, responsible for updating Mannequin and animating the player
	m_pAnimationComponent = m_pEntity->GetOrCreateComponent<Cry::DefaultComponents::CAdvancedAnimationComponent>();

	// Set the player geometry, this also triggers physics proxy creation
	m_pAnimationComponent->SetMannequinAnimationDatabaseFile("Animations/Mannequin/ADB/FirstPerson.adb");
	m_pAnimationComponent->SetCharacterFile("Objects/Characters/Hazmat/hazmat_man2.cdf");

	m_pAnimationComponent->SetControllerDefinitionFile("Animations/Mannequin/ADB/FirstPersonControllerDefinition.xml");
	m_pAnimationComponent->SetDefaultScopeContextName("FirstPersonCharacter");
	// Queue the idle fragment to start playing immediately on next update
	m_pAnimationComponent->SetDefaultFragmentName("Idle");

	// Disable movement coming from the animation (root joint offset), we control this entirely via physics
	m_pAnimationComponent->SetAnimationDrivenMotion(false);

	// Load the character and Mannequin data from file
	m_pAnimationComponent->LoadFromDisk();

	// Acquire tag identifiers to avoid doing so each update
	m_walkTagId = m_pAnimationComponent->GetTagId("Walk");

	// Mark the entity to be replicated over the network
	m_pEntity->GetNetEntity()->BindToNetwork();

	// Register the RemoteReviveOnClient function as a Remote Method Invocation (RMI) that can be executed by the server on clients
	SRmi<RMI_WRAP(&CPlayerComponent::RemoteReviveOnClient)>::Register(this, eRAT_NoAttach, false, eNRT_ReliableOrdered);
}

void CPlayerComponent::InitializeLocalPlayer()
{
	//Spawn the Camera
	SEntitySpawnParams sp;
	sp.vPosition = m_pEntity->GetWorldPos();
	sp.qRotation = IDENTITY;
	sp.pClass = gEnv->pEntitySystem->GetClassRegistry()->GetDefaultClass();
	sp.sName = "Camera";

	m_pCameraEntity = gEnv->pEntitySystem->SpawnEntity(sp);

	//Rotate the camera towards the player (180 in example, -90 in level1)
	m_pCameraEntity->SetRotation(Quat::CreateRotationZ(DEG2RAD(180)) * m_pCameraEntity->GetWorldRotation());

	// Create the camera component, will automatically update the viewport every frame
	m_pCameraComponent = m_pCameraEntity->GetOrCreateComponent<Cry::DefaultComponents::CCameraComponent>();

	// Create the audio listener component.
	m_pAudioListenerComponent = m_pCameraEntity->GetOrCreateComponent<Cry::Audio::DefaultComponents::CListenerComponent>();

	// Get the input component, wraps access to action mapping so we can easily get callbacks when inputs are triggered
	m_pInputComponent = m_pEntity->GetOrCreateComponent<Cry::DefaultComponents::CInputComponent>();

	// Register an action, and the callback that will be sent when it's triggered
	m_pInputComponent->RegisterAction("player", "moveleft", [this](int activationMode, float value) { HandleInputFlagChange(EInputFlag::MoveLeft, (EActionActivationMode)activationMode);  });
	// Bind the 'A' key the "moveleft" action
	m_pInputComponent->BindAction("player", "moveleft", eAID_KeyboardMouse, EKeyId::eKI_A);

	m_pInputComponent->RegisterAction("player", "moveright", [this](int activationMode, float value) { HandleInputFlagChange(EInputFlag::MoveRight, (EActionActivationMode)activationMode);  });
	m_pInputComponent->BindAction("player", "moveright", eAID_KeyboardMouse, EKeyId::eKI_D);

	m_pInputComponent->RegisterAction("player", "jump", [this](int activationMode, float value)
	{
		// Only jump if the button was pressed
		if (activationMode == eAAM_OnPress)
		{
			if (m_pCharacterController->IsOnGround())
				m_pCharacterController->AddVelocity(Vec3(0, 0, 5.f));
		}
	});

	m_pInputComponent->BindAction("player", "jump", eAID_KeyboardMouse, EKeyId::eKI_Space);

	//Truck Player
	if (m_playerID == 0)
	{
		// Create the camera component, will automatically update the viewport every frame
		m_pCameraComponent = m_pCameraEntity->GetOrCreateComponent<Cry::DefaultComponents::CCameraComponent>();

		m_pInputComponent->RegisterAction("vehicle", "enterVehicle", [this](int activationMode, float value) {

			if (activationMode == eAAM_OnPress)
			{
				IEntity* pVehicleEntity = gEnv->pEntitySystem->FindEntityByName("Truck");

				if (pVehicleEntity)
				{
					/*CVehicleComponent* pVehicle = pVehicleEntity->GetComponent<CVehicleComponent>();

					if (!pVehicle->PlayerIsDriver())
					{
						pVehicle->SetDriver(GetPlayerComponent());
					}
					else {
						pVehicle->DettachDriver(GetPlayerComponent());
					}*/
				}
			}
		});
		m_pInputComponent->BindAction("vehicle", "enterVehicle", eAID_KeyboardMouse, EKeyId::eKI_E);
	}

	m_pInputComponent->RegisterAction("player", "jump", [this](int activationMode, float value)
	{
		// Only jump if the button was pressed
		if (activationMode == eAAM_OnPress)
		{
			if (m_pCharacterController->IsOnGround())
				m_pCharacterController->AddVelocity(Vec3(0, 0, 5.f));
		}
	});

	m_pInputComponent->BindAction("player", "jump", eAID_KeyboardMouse, EKeyId::eKI_Space);

	//BoxPlayer (normally ID == 1)
	//TODO: change this later
	if (m_playerID == 0)
	{
		m_pInputComponent->RegisterAction("player", "attach_throw", [this](int activationMode, float value)
		{
			const float frameTime = gEnv->pTimer->GetFrameTime();

			switch (activationMode)
			{
			case EActionActivationMode::eAAM_OnHold:
			{
				if (m_allowThrowingBox)
				{
					m_boxThrowForce += m_throwForceAcceleration * frameTime;
					m_boxThrowForce = CLAMP(m_boxThrowForce, 0.f, 5.f);

					//m_allowDrawLine = true;

					CGamePlugin::GetInstance()->m_pServer->DrawTrajectoryOnClients(m_pEntity->GetForwardDir() * m_boxThrowForce);
				}

				break;
			}
			case EActionActivationMode::eAAM_OnRelease:
			{
				if (m_allowThrowingBox)
				{
					CGamePlugin::GetInstance()->m_pServer->StopTrajectoryRenderingOnClients();

					const Vec3 throwForceXDir = m_pEntity->GetForwardDir() * m_boxThrowForce;
					CGamePlugin::GetInstance()->m_pServer->ThrowBoxOnServer(throwForceXDir);

					//reset
					m_boxThrowForce = ZERO;
					m_allowThrowingBox = false;

					//ps.m_boxCount--;

					goto end;
				}

				//if the player has no box:
				else
				{
					//Look for Box!
					ray_hit forwardRay = Raycast(m_pEntity->GetWorldPos() + Vec3(0.f, 0.f, 0.5), m_pEntity->GetForwardDir());

					//Debug Draw
					/*if (forwardRay.pCollider)
					{
						gEnv->pAuxGeomRenderer->DrawLine(m_pEntity->GetWorldPos() + Vec3(0.f, 0.f, 0.5), ColorB(0, 0, 255, 255), forwardRay.pt, ColorB(0, 0, 255, 255));
					}*/
					//~Debug Draw

					if (forwardRay.pCollider)
					{
						IEntity* pOtherEntity = (IEntity*)forwardRay.pCollider->GetForeignData(PHYS_FOREIGN_ID_ENTITY);

						if (pOtherEntity)
						{
							//if it is a box, attach it to the player
							if (CBoxComponent* pBoxComp = pOtherEntity->GetComponent<CBoxComponent>())
							{
								ICharacterInstance* pCharInstance = m_pAnimationComponent->GetCharacter();

								IAttachmentManager* pAttachmentManager = pCharInstance->GetIAttachmentManager();
								IAttachment* pAttachment = pAttachmentManager->GetInterfaceByName("#box");


								if (pAttachment == nullptr)
								{
									CRY_ASSERT(pAttachment, "Check your character setup");
									return;
								}

								//Set Box to correct position
								QuatTS ts = pAttachment->GetAttWorldAbsolute();

								CGamePlugin::GetInstance()->m_pServer->AttachBoxOnServer(ts.t);
								m_allowThrowingBox = true;
							}
						}
					}
				}

				break;
			}
			}
			

			//prevent the player from picking up another box, when he's looking at the BoxSpawnPoint
		end:
			;
		});
		m_pInputComponent->BindAction("player", "attach_throw", eAID_KeyboardMouse, EKeyId::eKI_Mouse1);

		//Debug
		m_pInputComponent->RegisterAction("player", "spawnBox", [this](int activationMode, float value)
		{
			if (activationMode == eAAM_OnPress)
			{
				//CBoxComponent* pBox = CBoxComponent::SpawnBox(m_pEntity->GetWorldPos());
				//pBox->AttachBoxToPlayer(m_pAnimationComponent);

				CGamePlugin::GetInstance()->m_pServer->SpawnOnServer(m_pEntity->GetWorldPos());
			}
		});
		m_pInputComponent->BindAction("player", "spawnBox", eAID_KeyboardMouse, EKeyId::eKI_F);
		//~Debug
	}

	m_pInputComponent->RegisterAction("player", "connect", [this](int activationMode, float value)
	{
		if (activationMode == eAAM_OnPress)
		{
			gEnv->pConsole->ExecuteString("connect 192.168.178.46", false, true);
		}
	});
	m_pInputComponent->BindAction("player", "connect", eAID_KeyboardMouse, EKeyId::eKI_C);

}

std::vector<IPhysicalEntity*> CPlayerComponent::GetAllColider()
{
	std::vector<IPhysicalEntity*> physicalEntities;

	if (m_pEntity->GetPhysicalEntity() != nullptr)
	{
		physicalEntities.push_back(m_pEntity->GetPhysicalEntity());
	}

	if (m_pAnimationComponent != nullptr)
	{
		ICharacterInstance* pCharacterInstance = m_pAnimationComponent->GetCharacter();
		if (pCharacterInstance)
		{
			ISkeletonPose* pSkeletonPose = pCharacterInstance->GetISkeletonPose();
			if (pSkeletonPose)
			{
				physicalEntities.push_back(pSkeletonPose->GetCharacterPhysics());
			}
		}
	}

	return physicalEntities;
}

ray_hit CPlayerComponent::Raycast(Vec3 origin, Vec3 dir)
{
	ray_hit rayhit;
	//make sure the hit collider is not initialized with random stuff
	rayhit.pCollider = nullptr;

	std::vector<IPhysicalEntity*> physicalEntities = GetAllColider();

	int nSkipEnt = physicalEntities.size();

	if (gEnv->pPhysicalWorld->RayWorldIntersection(origin, dir, ent_all, rwi_stop_at_pierceable | rwi_colltype_any, &rayhit, 1, &physicalEntities[0], nSkipEnt))
	{
		//gEnv->pRenderer->GetIRenderAuxGeom()->Draw2dLabel(400.5f, 200.f, 1.5f, ColorF(0.f, 1.f, 0.f, 1.f), false, "hit!!");
	}

	return rayhit;
}

Cry::Entity::EventFlags CPlayerComponent::GetEventMask() const
{
	return
		Cry::Entity::EEvent::BecomeLocalPlayer |
		Cry::Entity::EEvent::GameplayStarted |
		Cry::Entity::EEvent::Update |
		Cry::Entity::EEvent::Reset;
}

void CPlayerComponent::ProcessEvent(const SEntityEvent& event)
{
	switch (event.event)
	{
	case Cry::Entity::EEvent::BecomeLocalPlayer:
	{
		InitializeLocalPlayer();
	}
	break;
	case Cry::Entity::EEvent::GameplayStarted:
	{
		const Matrix34 newTransform = CSpawnPointComponent::GetFirstSpawnPointTransform();

		Revive(newTransform);
	}
	break;
	case Cry::Entity::EEvent::Update:
	{
		// Don't update the player if we haven't spawned yet
		if (!m_isAlive)
			return;

		const float frameTime = event.fParam[0];

		// Start by updating the movement request we want to send to the character controller
		// This results in the physical representation of the character moving
		UpdateMovementRequest(frameTime);

		// Update the animation state of the character
		UpdateAnimation(frameTime);

		if (IsLocalClient())
		{
			// Update the camera component offset
			UpdateCamera(frameTime);

			if (m_allowDrawLine)
			{
				if (m_trajectoryPoints.size() > 0)
				{
					gEnv->pRenderer->GetIRenderAuxGeom()->DrawLines(&m_trajectoryPoints[0], m_trajectoryPoints.size(), Col_BlueViolet);
				}
			}
		}
	}
	break;
	case Cry::Entity::EEvent::Reset:
	{
		// Disable player when leaving game mode.
		m_isAlive = event.nParam[0] != 0;
	}
	break;
	}
}

void CPlayerComponent::NetReplicateSerialize(TSerialize ser)
{
	ser.Value("playerID", m_playerID);
}

bool CPlayerComponent::NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags)
{
	if (aspect == InputAspect)
	{
		ser.BeginGroup("PlayerInput");

		const CEnumFlags<EInputFlag> prevInputFlags = m_inputFlags;

		ser.Value("m_inputFlags", m_inputFlags.UnderlyingValue(), 'ui8');

		if (ser.IsReading())
		{
			const CEnumFlags<EInputFlag> changedKeys = prevInputFlags ^ m_inputFlags;

			const CEnumFlags<EInputFlag> pressedKeys = changedKeys & prevInputFlags;
			if (!pressedKeys.IsEmpty())
			{
				HandleInputFlagChange(pressedKeys, eAAM_OnPress);
			}

			const CEnumFlags<EInputFlag> releasedKeys = changedKeys & prevInputFlags;
			if (!releasedKeys.IsEmpty())
			{
				HandleInputFlagChange(pressedKeys, eAAM_OnRelease);
			}
		}

		ser.EndGroup();
	}

	return true;
}

void CPlayerComponent::UpdateMovementRequest(float frameTime)
{
	// Don't handle input if we are in air
	if (!m_pCharacterController->IsOnGround())
		return;

	Vec3 velocity = ZERO;

	const float moveSpeed = 20.5f;

	if (m_inputFlags & EInputFlag::MoveLeft)
	{
		velocity.y -= moveSpeed * frameTime;
	}
	if (m_inputFlags & EInputFlag::MoveRight)
	{
		velocity.y += moveSpeed * frameTime;
	}

	m_pCharacterController->AddVelocity(GetEntity()->GetWorldRotation() * velocity);
}

void CPlayerComponent::UpdateAnimation(float frameTime)
{
	// Update the Mannequin tags
	m_pAnimationComponent->SetTagWithId(m_walkTagId, m_pCharacterController->IsWalking());
}

void CPlayerComponent::UpdateCamera(float frameTime)
{
	const Vec3 offsetToPlayer = Vec3(10.f, 0.f, 2.f);

	Vec3 originCamera = ZERO;

	if (m_pVehicle)
	{
		originCamera = m_pVehicle->GetWorldPos();
	}
	else
	{
		originCamera = m_pEntity->GetWorldPos();
	}

	const Vec3 camOffset = originCamera + m_pEntity->GetRightDir() * offsetToPlayer.x + Vec3(0.f, 0.f, 1.f) * offsetToPlayer.z;
	m_pCameraEntity->SetPos(camOffset);
}

void CPlayerComponent::OnReadyForGameplayOnServer()
{
	CRY_ASSERT(gEnv->bServer, "This function should only be called on the server!");

	const Matrix34 newTransform = CSpawnPointComponent::GetFirstSpawnPointTransform();
	Revive(newTransform);

	// Invoke the RemoteReviveOnClient function on all remote clients, to ensure that Revive is called across the network
	SRmi<RMI_WRAP(&CPlayerComponent::RemoteReviveOnClient)>::InvokeOnOtherClients(this, RemoteReviveParams{ newTransform.GetTranslation(), Quat(newTransform) });

	// Go through all other players, and send the RemoteReviveOnClient on their instances to the new player that is ready for gameplay
	const int channelId = m_pEntity->GetNetEntity()->GetChannelId();
	CGamePlugin::GetInstance()->IterateOverPlayers([this, channelId](CPlayerComponent& player)
	{
		// Don't send the event for the player itself (handled in the RemoteReviveOnClient event above sent to all clients)
		if (player.GetEntityId() == GetEntityId())
			return;

		// Only send the Revive event to players that have already respawned on the server
		if (!player.m_isAlive)
			return;

		// Revive this player on the new player's machine, on the location the existing player was currently at
		const QuatT currentOrientation = QuatT(player.GetEntity()->GetWorldTM());
		SRmi<RMI_WRAP(&CPlayerComponent::RemoteReviveOnClient)>::InvokeOnClient(&player, RemoteReviveParams{ currentOrientation.t, currentOrientation.q }, channelId);
	});
}

void CPlayerComponent::SetPlayerID(int id)
{
	m_playerID = id;
}

bool CPlayerComponent::RemoteReviveOnClient(RemoteReviveParams&& params, INetChannel* pNetChannel)
{
	// Call the Revive function on this client
	Revive(Matrix34::Create(Vec3(1.f), params.rotation, params.position));

	return true;
}

void CPlayerComponent::Revive(const Matrix34& transform)
{
	m_isAlive = true;

	m_pEntity->SetWorldTM(transform);

	// Apply the character to the entity and queue animations
	m_pAnimationComponent->ResetCharacter();
	m_pCharacterController->Physicalize();

	// Reset input now that the player respawned
	m_inputFlags.Clear();
	NetMarkAspectsDirty(InputAspect);

	// Reset trajectory for drawing
	m_allowDrawLine = false;
	m_boxThrowForce = ZERO;
}

void CPlayerComponent::HandleInputFlagChange(const CEnumFlags<EInputFlag> flags, const CEnumFlags<EActionActivationMode> activationMode, const EInputFlagType type)
{
	switch (type)
	{
	case EInputFlagType::Hold:
	{
		if (activationMode == eAAM_OnRelease)
		{
			m_inputFlags &= ~flags;
		}
		else
		{
			m_inputFlags |= flags;
		}
	}
	break;
	case EInputFlagType::Toggle:
	{
		if (activationMode == eAAM_OnRelease)
		{
			// Toggle the bit(s)
			m_inputFlags ^= flags;
		}
	}
	break;
	}

	if (IsLocalClient())
	{
		NetMarkAspectsDirty(InputAspect);
	}
}
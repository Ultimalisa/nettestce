// Copyright 2001-2019 Crytek GmbH / Crytek Group. All rights reserved.

#pragma once

#include <CryNetwork/Rmi.h>
#include "Components/Box/BoxComponent.h"

class CServerComponent final : public IEntityComponent
{
public:
	CServerComponent();
	virtual ~CServerComponent() = default;

	// Reflect type to set a unique identifier for this component
	// and provide additional information to expose it in the sandbox
	static void ReflectType(Schematyc::CTypeDesc<CServerComponent>& desc)
	{
		desc.SetGUID("{231B9773-63C2-4A11-BBAD-2E400DF10C9E}"_cry_guid);
	}

	virtual void Initialize() override;

	//static CServerComponent* GetServerComponent();

	virtual bool NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags) override;
	virtual void NetReplicateSerialize(TSerialize ser) override;

	//Spawn Box on Server & Client RMI
	//only share position
	struct SBoxParams
	{
		Vec3 spawnPos;
		void SerializeWith(TSerialize ser)
		{
			ser.Value("pos", spawnPos, 'wrld');
		}
	};

	void SpawnOnServer(Vec3 spawnPos);
	bool RemoteSpawnOnServer(SBoxParams&& p, INetChannel* pNetChannel);

	// Use a type definition to simply registering and invoking the RMI
	using ServerSpawnBoxRMI = SRmi<RMI_WRAP(&CServerComponent::RemoteSpawnOnServer)>;

	struct SAttachmentProperties
	{
		Vec3 attachPos;
		void SerializeWith(TSerialize ser)
		{
			ser.Value("attachPos", attachPos, 'wrl3');
		}
	};

	struct SEmpty
	{
		void SerializeWith(TSerialize ser)
		{
		}
	};

	void AttachBoxOnServer(Vec3 attachmentPos);
	bool RemoteAttachBoxToPlayer(SAttachmentProperties&& p, INetChannel* pNetChannel);
	
	using ServerAttachBoxRMI = SRmi<RMI_WRAP(&CServerComponent::RemoteAttachBoxToPlayer)>;

	struct SThrowBox
	{
		Vec3 dirXForce;
		void SerializeWith(TSerialize ser)
		{
			ser.Value("ThrowDirectionXForce", dirXForce, 'lwld');
		}
	};

	void ThrowBoxOnServer(Vec3 dirXForce);
	bool RemoteThrowPlayerBox(SThrowBox&& tb, INetChannel* pNetChannel);

	using ServerThrowBoxRMI = SRmi<RMI_WRAP(&CServerComponent::RemoteThrowPlayerBox)>;

	void DrawTrajectoryOnClients(Vec3 dirXForce);
	bool RemoteDrawTrajectory(SThrowBox&& tb, INetChannel* pNetChannel);

	using ServerDrawTrajectoryRMI = SRmi<RMI_WRAP(&CServerComponent::RemoteDrawTrajectory)>;

	void StopTrajectoryRenderingOnClients();
	bool RemoteStopTrajectoryRender(SEmpty&& p, INetChannel* pNetChannel);

	using ServerStopTrajectoryRenderRMI = SRmi<RMI_WRAP(&CServerComponent::RemoteStopTrajectoryRender)>;

public:
	std::vector<CBoxComponent*> m_pBoxV;
	std::vector<CPlayerComponent*> m_pPlayerV;
};

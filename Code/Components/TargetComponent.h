#pragma once
#include <stdAfx.h>

enum ETargetValue
{
	eLowValueTarget = 10,
	eModerateValueTarget = 30,
	eHighValueTarget = 50
};

enum EAreaFilter
{
	Box,
	Player
};

enum EAreaType
{
	BoxT,
	Sphere
};

class CTargetComponent : public IEntityComponent, public IEntityComponentPreviewer, public IAreaManagerEventListener
{
public:
	CTargetComponent() = default;
	virtual ~CTargetComponent();

	// IEntityComponent
	virtual void Initialize() override;
	virtual IEntityComponentPreviewer* GetPreviewer() final { return this; }
	virtual void SerializeProperties(Serialization::IArchive& archive) final {}
	virtual void ProcessEvent(const SEntityEvent& event) override;

	virtual Cry::Entity::EventFlags GetEventMask() const override
	{
		return EEntityEvent::EntityEnteredThisArea | EEntityEvent::EntityLeftThisArea | EEntityEvent::EditorPropertyChanged;
	}
	// ~IEntityComponent

	// Reflect type to set a unique identifier for this component
	static void ReflectType(Schematyc::CTypeDesc<CTargetComponent>& desc)
	{
		desc.SetEditorCategory("Custom");
		desc.SetLabel("Target Component");
		desc.SetGUID("{C5B5D950-C356-4FB9-946C-914CE2869C74}"_cry_guid);
		desc.SetComponentFlags({ IEntityComponent::EFlags::Transform, IEntityComponent::EFlags::Socket, IEntityComponent::EFlags::Attach, IEntityComponent::EFlags::UserAdded });

		desc.AddMember(&CTargetComponent::m_value, 'val', "TargetValue", "Target Value", "", ETargetValue::eModerateValueTarget);
		desc.AddMember(&CTargetComponent::m_sAreaTrigger, 'sat', "AreaTrigger", "Area Trigger", "", SAreaTrigger());
	}

	struct SAreaTrigger
	{
		inline bool operator==(const SAreaTrigger &rhs) const { return 0 == memcmp(this, &rhs, sizeof(rhs)); }
		static void ReflectType(Schematyc::CTypeDesc<SAreaTrigger>& desc)
		{
			desc.SetGUID("{19FE03C3-4E97-4557-844D-CCC842E96D88}"_cry_guid);
			desc.SetLabel("Area Trigger");
			desc.AddMember(&CTargetComponent::SAreaTrigger::m_areaFilter, 'af', "AreaFilter", "Area Filter", "", EAreaFilter::Box);
			desc.AddMember(&CTargetComponent::SAreaTrigger::m_areaType, 'at', "AreaType", "Area Type", "", EAreaType::BoxT);
			desc.AddMember(&CTargetComponent::SAreaTrigger::m_vPositionalOffset, 'vOff', "PosOffset", "Positional offset", "The positional offset of this collider", Vec3(0.f, 0.f, 0.f));
			desc.AddMember(&CTargetComponent::SAreaTrigger::m_triggerSize, 'tbs', "BoxSize", "Box Size", "", Vec3(1.0f, 1.0f, 1.0f));
			desc.AddMember(&CTargetComponent::SAreaTrigger::m_fRadius, 'rad', "Radius", "Radius", "", 1.f);
			desc.AddMember(&CTargetComponent::SAreaTrigger::m_bActive, 'ba', "Active", "Active", "", true);

		}

		EAreaFilter m_areaFilter = EAreaFilter::Box;
		EAreaType m_areaType = EAreaType::BoxT;

		Vec3 m_triggerSize = Vec3(1, 1, 1);
		bool m_bActive = true;
		Schematyc::PositiveFloat m_fRadius = 1.f;
		Vec3 m_vPositionalOffset = ZERO;
	};

	virtual void OnAreaManagerEvent(EEntityEvent event, EntityId TriggerEntityID, IArea* pArea) override;

	virtual void Render(const IEntity& entity, const IEntityComponent& component, SEntityPreviewContext &context) const final;

protected:
	void InvalidateTrigger();

protected:
	IEntityAreaComponent* m_pAreaComponent = nullptr;
	ETargetValue m_value = ETargetValue::eModerateValueTarget;
	SAreaTrigger m_sAreaTrigger;
};
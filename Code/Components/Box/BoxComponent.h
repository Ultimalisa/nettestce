#pragma once

#include "IBox.h"
#include <CryNetwork/Rmi.h>


class CPlayerComponent;

class CBoxComponent : public IEntityComponent, IBox
{
public:
	CBoxComponent() = default;
	virtual ~CBoxComponent() = default;

	// IEntityComponent
	virtual void Initialize() override;

	virtual Cry::Entity::EventFlags GetEventMask() const
	{
		// ENTITY_EVENT_BIT wandelt den enum in eine Bit Flag um.
		// Wollen wir mehrere Enums kombinieren benutzen wir '|' <-- bit operator

		return ENTITY_EVENT_BIT(EEntityEvent::Update);
	}
	virtual void ProcessEvent(const SEntityEvent& event) override;
	// ~IEntityComponent

	// Reflect type to set a unique identifier for this component
	static void ReflectType(Schematyc::CTypeDesc<CBoxComponent>& desc)
	{
		desc.SetGUID("{6D2527B3-B027-46F5-A062-549349AAB2B2}"_cry_guid);
	}

	void AttachBoxToPlayer(Vec3 attachPos, IEntity* pPlayerEnt);
	void ThrowBox(Vec3 dirXforce);

	std::vector<Vec3> GetPositionPoints(Vec3 dirXForce);
	inline float GetLifetime() { return m_lifeTime; };
	Vec3 GetBoxSize();
	float GetMass() { return m_mass; };

private:
	void Physicalize();

	struct SEmpty
	{
		void SerializeWith(TSerialize ser)
		{
		}
	};

	void RemoveBoxOnServer();
	bool RemoteRemoveBox(SEmpty&& p, INetChannel* pNetChannel);

	using ServerRemoveBoxRMI = SRmi<RMI_WRAP(&CBoxComponent::RemoteRemoveBox)>;

private:
	bool m_isAlive = true;
	//maxsize of positions that will be calculated for the trajectory
	unsigned int m_trajectorySizeInOut = 100;

	Vec3 m_boxSize = ZERO;
};
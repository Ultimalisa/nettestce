#include "StdAfx.h"

#include "FakeBoxComponent.h"
#include <CryPhysics/physinterface.h>
#include "GamePlugin.h"

static void RegisterFakeBoxComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CFakeBoxComponent));
	}
}

CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterFakeBoxComponent);

void CFakeBoxComponent::Initialize()
{
	// Set the model
	const int geometrySlot = 0;
	m_pEntity->LoadGeometry(geometrySlot, "Objects/architecture/box_1m/box_1m.cgf");

	m_pEntity->SetFlags(m_pEntity->GetFlags() | EEntityFlags::ENTITY_FLAG_TRIGGER_AREAS);

	Physicalize();

	// Get correct box size
	pe_status_pos sp;
	sp.ipart = 0;
	sp.partid = 0;

	m_pEntity->GetPhysics()->GetStatus(&sp);

	primitives::box b;
	sp.pGeom->GetBBox(&b);

	m_boxSize = b.size;
}

void CFakeBoxComponent::ProcessEvent(const SEntityEvent& event)
{
	if(event.event == EEntityEvent::Update)
	{	
		if (!gEnv->bServer)
		{
			const float frameTime = gEnv->pTimer->GetFrameTime();

			if (m_objectWasThrown)
			{
				m_lifeTime = CLAMP(m_lifeTime - frameTime, -1.f, m_lifeTimeMax);
			}

			if (m_lifeTime <= 0.f)
			{
				gEnv->pEntitySystem->RemoveEntity(GetEntityId());
			}
		}
	}
}

void CFakeBoxComponent::ThrowBox(Vec3 dirXforce)
{
	m_lifeTime = m_lifeTimeMax;

	pe_simulation_params sp;
	m_pEntity->GetPhysics()->GetParams(&sp);

	sp.damping = 1.f;
	sp.dampingFreefall = 0.1f;

	m_pEntity->GetPhysics()->SetParams(&sp);

	pe_action_set_velocity asv;
	asv.ipart = 0;
	asv.partid = 0;
	asv.v = dirXforce * m_mass;

	m_pEntity->GetPhysics()->Action(&asv);

	m_objectWasThrown = true;
}

void CFakeBoxComponent::Physicalize()
{
	SEntityPhysicalizeParams pp;
	pp.mass = m_mass;
	pp.type = PE_RIGID;

	m_pEntity->Physicalize(pp);
}

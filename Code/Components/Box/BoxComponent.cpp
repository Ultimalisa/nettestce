#include "StdAfx.h"

#include "BoxComponent.h"
#include <CryPhysics/physinterface.h>
#include "DefaultComponents/Geometry/StaticMeshComponent.h"
#include "../Player.h"
#include "GamePlugin.h"

static void RegisterBoxComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CBoxComponent));
	}
}

CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterBoxComponent);

void CBoxComponent::Initialize()
{
	// Set the model
	const int geometrySlot = 0;
	m_pEntity->LoadGeometry(geometrySlot, "Objects/architecture/box_1m/box_1m.cgf");

	m_pEntity->SetFlags(m_pEntity->GetFlags() | EEntityFlags::ENTITY_FLAG_TRIGGER_AREAS);

	Physicalize();

	//Register RemoveRMI
	ServerRemoveBoxRMI::Register(this, eRAT_NoAttach, true, eNRT_ReliableUnordered);

	// Get correct box size
	pe_status_pos sp;
	sp.ipart = 0;
	sp.partid = 0;

	m_pEntity->GetPhysics()->GetStatus(&sp);

	primitives::box b;
	sp.pGeom->GetBBox(&b);

	m_boxSize = b.size;

	CGamePlugin::GetInstance()->m_pServer->m_pBoxV.push_back(this);
	/*m_pEntity->GetNetEntity()->EnableDelegatableAspect(eEA_Physics, true);*/
}

void CBoxComponent::ProcessEvent(const SEntityEvent& event)
{
	if(event.event == EEntityEvent::Update)
	{
		if (!m_isAlive)
			return;

		if (!gEnv->bServer)
		{
			const float frameTime = gEnv->pTimer->GetFrameTime();

			if (m_objectWasThrown)
			{
				m_lifeTime = CLAMP(m_lifeTime - frameTime, -1.f, m_lifeTimeMax);
			}

			if (m_lifeTime <= 0.f)
			{
				RemoveBoxOnServer();
				m_isAlive = false;
			}
		}

		//const Vec3 pos = m_pEntity->GetWorldPos();
		//CryLogAlways("box position: %f, %f, %f", pos.x, pos.y, pos.z);
	}
}

void CBoxComponent::AttachBoxToPlayer(Vec3 attachPos, IEntity* pPlayerEnt)
{
	//ICharacterInstance* pCharInstance = animComponent->GetCharacter();

	//IAttachmentManager* pAttachmentManager = pCharInstance->GetIAttachmentManager();
	//IAttachment* pAttachment = pAttachmentManager->GetInterfaceByName("#box");


	//if (pAttachment == nullptr)
	//{
	//	CRY_ASSERT(pAttachment, "Check your character setup");
	//	return;
	//}

	////Set Box to correct position
	//QuatTS ts = pAttachment->GetAttWorldAbsolute();

	m_pEntity->SetPos(attachPos);
	m_pEntity->SetRotation(pPlayerEnt->GetWorldRotation());

	SEntityPhysicalizeParams pp;
	pp.type = PE_NONE;

	m_pEntity->Physicalize(pp);

	//Attach Box (via Child Attach)
	pPlayerEnt->AttachChild(m_pEntity, SChildAttachParams(IEntity::EAttachmentFlags::ATTACHMENT_KEEP_TRANSFORMATION));
}

void CBoxComponent::ThrowBox(Vec3 dirXforce)
{
	CGamePlugin::GetInstance()->m_pServer->m_pBoxV.pop_back();
	m_pEntity->DetachThis(IEntity::EAttachmentFlags::ATTACHMENT_KEEP_TRANSFORMATION);

	Physicalize();

	m_lifeTime = m_lifeTimeMax;

	pe_simulation_params sp;
	m_pEntity->GetPhysics()->GetParams(&sp);

	sp.damping = 1.f;
	sp.dampingFreefall = 0.1f;

	m_pEntity->GetPhysics()->SetParams(&sp);

	pe_action_set_velocity asv;
	asv.ipart = 0;
	asv.partid = 0;
	asv.v = dirXforce * m_mass;

	m_pEntity->GetPhysics()->Action(&asv);

	m_objectWasThrown = true;
}

std::vector<Vec3> CBoxComponent::GetPositionPoints(Vec3 dirXForce)
{
	primitives::box box;
	box.Basis = IDENTITY;
	box.bOriented = 1;
	box.center = Vec3(ZERO);
	box.size = m_boxSize;

	IGeometry* pGeom = gEnv->pPhysicalWorld->GetGeomManager()->CreatePrimitive(box.type, &box);

	pe_geomparams gp;
	gp.mass = m_mass;
	gp.density = 1.f;
	gp.bRecalcBBox = 1;
	gp.pos = box.center;
	gp.q = IDENTITY;
	gp.minContactDist = 0.01f;
	//gp.surface_idx = m_pMat->GetSurfaceTypeId();

	phys_geometry* pg = gEnv->pPhysicalWorld->GetGeomManager()->RegisterGeometry(pGeom);

	pe_params_pos pp;
	pp.pos = m_pEntity->GetWorldPos();
	pp.q = m_pEntity->GetWorldRotation();
	pp.scale = 1.f;

	IPhysicalEntity* rigid = gEnv->pPhysicalWorld->CreatePhysicalEntity(PE_RIGID, &pp);

	rigid->AddGeometry(pg, &gp);

	pe_params_part pap;
	rigid->GetParams(&pap);
	pap.flagsColliderAND = 0;
	rigid->SetParams(&pap);

	pe_simulation_params spa;
	rigid->GetParams(&spa);

	spa.damping = 1.f;
	spa.dampingFreefall = 0.1f;

	rigid->SetParams(&spa);


	// Trajectory
	const float timeStep = 0.1f;

	pe_status_pos sp;
	sp.partid = 0;
	sp.ipart = 0;

	pe_status_dynamics sd;
	sd.partid = 0;
	sd.ipart = 0;

	const Vec3 boxLaunchVelocity = dirXForce * m_mass;

	pe_action_set_velocity asv;
	asv.ipart = 0;
	asv.partid = 0;
	asv.v = boxLaunchVelocity;

	rigid->Action(&asv);

	uint stationary = 0;
	Vec3 lastPosition = m_pEntity->GetWorldPos();
	
	std::vector<Vec3> trajectoryPositionsV; 
	trajectoryPositionsV.reserve(m_trajectorySizeInOut);

	std::vector<Vec3> trajectoryVelocitiesV; 
	trajectoryVelocitiesV.reserve(m_trajectorySizeInOut);

	trajectoryPositionsV.push_back(lastPosition);
	trajectoryVelocitiesV.push_back(boxLaunchVelocity);

	//Counter starts at 1, since current box position is first entry of vector
	unsigned int n = 1;

	for (float t = 0.0f; t < m_lifeTime; t += timeStep)
	{
		if (n == m_trajectorySizeInOut)
			break;

		rigid->DoStep(timeStep);

		rigid->GetStatus(&sp);
		rigid->GetStatus(&sd);

		Vec3 position = sp.pos;
		const Vec3 velocity = sd.v;

		float distSq = Distance::Point_PointSq(lastPosition, position);
		lastPosition = position;

		// Early out when almost stationary.
		if (distSq < sqr(0.01f))
			stationary++;
		else
			stationary = 0;
		if (stationary > 2)
			break;

		trajectoryPositionsV.push_back(position);
		trajectoryVelocitiesV.push_back(velocity);
		++n;
		
	}
	// ~Trajectory

	gEnv->pPhysicalWorld->DestroyPhysicalEntity(rigid);

	return trajectoryPositionsV;
}

Vec3 CBoxComponent::GetBoxSize()
{
	return m_boxSize;
}

void CBoxComponent::Physicalize()
{
	SEntityPhysicalizeParams pp;
	pp.mass = m_mass;
	pp.type = PE_RIGID;

	m_pEntity->Physicalize(pp);
}

void CBoxComponent::RemoveBoxOnServer()
{
	if (!gEnv->bServer)
	{
		ServerRemoveBoxRMI::InvokeOnServer(this, SEmpty{});
	}
}

bool CBoxComponent::RemoteRemoveBox(SEmpty && p, INetChannel * pNetChannel)
{
	if (gEnv->bServer)
	{
		//m_pEntity->DetachThis();

		CryLogAlways("Deleted");
		m_isAlive = false;
		gEnv->pEntitySystem->RemoveEntity(GetEntityId(), true);
	}

	return true;
}

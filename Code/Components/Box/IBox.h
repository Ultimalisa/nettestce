#pragma once
#include <stdAfx.h>

#include <DefaultComponents/Geometry/AdvancedAnimationComponent.h>

class IBox
{

protected:
	float m_mass = 20.f;
	float m_lifeTime = 5.f;
	float m_lifeTimeMax = 5.f;
	bool m_objectWasThrown = false;
};
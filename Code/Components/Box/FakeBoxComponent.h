#pragma once

#include "IBox.h"

class CFakeBoxComponent : public IEntityComponent, IBox
{
public:
	CFakeBoxComponent() = default;
	virtual ~CFakeBoxComponent() = default;

	// IEntityComponent
	virtual void Initialize() override;

	virtual Cry::Entity::EventFlags GetEventMask() const
	{
		// ENTITY_EVENT_BIT wandelt den enum in eine Bit Flag um.
		// Wollen wir mehrere Enums kombinieren benutzen wir '|' <-- bit operator

		return ENTITY_EVENT_BIT(EEntityEvent::Update);
	}
	virtual void ProcessEvent(const SEntityEvent& event) override;

	// Reflect type to set a unique identifier for this component
	static void ReflectType(Schematyc::CTypeDesc<CFakeBoxComponent>& desc)
	{
		desc.SetGUID("{0BDAE1BC-3E46-44D6-B17A-7AB07E9E3CF0}"_cry_guid);
	}

	void ThrowBox(Vec3 dirXforce);

	inline float GetLifetime() { return m_lifeTime; };
	float GetMass() { return m_mass; };

private:
	void Physicalize();

private:
	Vec3 m_boxSize = ZERO;
};
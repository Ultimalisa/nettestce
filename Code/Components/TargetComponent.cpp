#include "TargetComponent.h"

static void RegisterComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CTargetComponent));
	}
}

CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterComponent);

static void ReflectType(Schematyc::CTypeDesc<ETargetValue>& desc)
{
	desc.SetGUID("{242CC77E-B9F0-409D-B0FC-7599EA26A9A3}"_cry_guid);
	desc.SetLabel("Target Value Enum");

	desc.SetDefaultValue(ETargetValue::eModerateValueTarget);
	desc.AddConstant(ETargetValue::eLowValueTarget, "LowValueTarget", "Low Value Target");
	desc.AddConstant(ETargetValue::eModerateValueTarget, "ModerateValueTarget", "Moderate Value Target");
	desc.AddConstant(ETargetValue::eHighValueTarget, "HighValueTarget", "High Value Target");
}

static void ReflectType(Schematyc::CTypeDesc<EAreaFilter>& desc)
{
	desc.SetGUID("{B0875021-C875-4301-9A4C-13711D4666BA}"_cry_guid);
	desc.SetLabel("Area Trigger Filter");

	desc.SetDefaultValue(EAreaFilter::Box);
	desc.AddConstant(EAreaFilter::Box, "Box", "Box");
	desc.AddConstant(EAreaFilter::Player, "Player", "Player");
}

static void ReflectType(Schematyc::CTypeDesc<EAreaType>& desc)
{
	desc.SetGUID("{F63B8EE2-16B9-4943-A21B-F0C6FA4AA56A}"_cry_guid);
	desc.SetLabel("Area Trigger Type");

	desc.SetDefaultValue(EAreaType::BoxT);
	desc.AddConstant(EAreaType::BoxT, "Box", "Box");
	desc.AddConstant(EAreaType::Sphere, "Sphere", "Sphere");
}

CTargetComponent::~CTargetComponent()
{
	gEnv->pEntitySystem->GetAreaManager()->RemoveEventListener(this);
}

void CTargetComponent::Initialize()
{
	m_pAreaComponent = m_pEntity->GetOrCreateComponent<IEntityAreaComponent>();
	InvalidateTrigger();

	gEnv->pEntitySystem->GetAreaManager()->AddEventListener(this);

	m_pEntity->GetNetEntity()->BindToNetwork();
}

void CTargetComponent::ProcessEvent(const SEntityEvent & event)
{
	if (event.event == EEntityEvent::EditorPropertyChanged)
	{
		InvalidateTrigger();
	}
}

void CTargetComponent::OnAreaManagerEvent(EEntityEvent event, EntityId TriggerEntityID, IArea * pArea)
{
	if (pArea != m_pAreaComponent->GetArea())
		return;

	if (event == ENTITY_EVENT_ENTERAREA && m_sAreaTrigger.m_bActive)
	{
		IEntity* pEntity = gEnv->pEntitySystem->GetEntity(TriggerEntityID);
		if (pEntity == nullptr)
			return;

		if (m_sAreaTrigger.m_areaFilter == EAreaFilter::Box)
		{
			if (pEntity->GetComponent<CBoxComponent>())
			{
				//increase score

				CryLogAlways("Box entered");
			}
		}
		//Area Filter is type Player
		else
		{
			if (pEntity->GetComponent<CPlayerComponent>())
			{
				CryLogAlways("Player entered");
			}
		}
	}
	else if (event == ENTITY_EVENT_LEAVEAREA && m_sAreaTrigger.m_bActive)
	{
		IEntity* pEntity = gEnv->pEntitySystem->GetEntity(TriggerEntityID);
		if (pEntity == nullptr)
			return;

		if (m_sAreaTrigger.m_areaFilter == EAreaFilter::Box)
		{
			if (pEntity->GetComponent<CBoxComponent>())
			{
				//DON'T increase score

				CryLogAlways("Box left");
			}
		}
		//Area Filter is type Player
		else
		{
			if (pEntity->GetComponent<CPlayerComponent>())
			{
				CryLogAlways("Player left");
			}
		}
	}
}

void CTargetComponent::Render(const IEntity & entity, const IEntityComponent & component, SEntityPreviewContext & context) const
{
	if (m_sAreaTrigger.m_areaType == EAreaType::Sphere)
	{
		gEnv->pAuxGeomRenderer->DrawSphere(entity.GetWorldPos(), m_sAreaTrigger.m_fRadius, Col_Red, EBoundingBoxDrawStyle::eBBD_Extremes_Color_Encoded);
	}
	else if (m_sAreaTrigger.m_areaType == EAreaType::BoxT)
	{
		AABB aabb;
		aabb.min = -m_sAreaTrigger.m_triggerSize / 2;
		aabb.max = m_sAreaTrigger.m_triggerSize / 2;

		gEnv->pAuxGeomRenderer->DrawAABB(aabb, m_pEntity->GetWorldTM(), false, Col_Red, EBoundingBoxDrawStyle::eBBD_Extremes_Color_Encoded);
	}
}

void CTargetComponent::InvalidateTrigger()
{
	if (m_sAreaTrigger.m_bActive == true)
	{
		// Set the offset translation of the component
		m_pTransform->SetTranslation(m_sAreaTrigger.m_vPositionalOffset);

		// Get the translation of this component
		const Vec3 componentTranslation = this->m_pTransform->GetTranslation();

		switch (m_sAreaTrigger.m_areaType)
		{
		case EAreaType::Sphere:

			if (m_pAreaComponent != nullptr)
			{
				m_pAreaComponent->SetSphere(componentTranslation, m_sAreaTrigger.m_fRadius);
			}
			break;

		case EAreaType::BoxT:

			// Remove tracked entities (which is our component)
			// We don't want to get notified if someone e.g. enters the sphere (see above!)
			if (m_pAreaComponent != nullptr)
			{
				// Workaround since we have to set something for the audio system
				bool a[6] = { 0 };

				m_pAreaComponent->SetBox(-m_sAreaTrigger.m_triggerSize / 2.f, m_sAreaTrigger.m_triggerSize / 2.f, a, 6);
			}
			break;
		}
	}
}

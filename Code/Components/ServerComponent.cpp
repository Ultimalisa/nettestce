#include "StdAfx.h"

#include "ServerComponent.h"
#include "Components/Box/BoxComponent.h"
#include "Components/Box/FakeBoxComponent.h"
#include "Player.h"

static void RegisterServerComponent(Schematyc::IEnvRegistrar& registrar)
{
	Schematyc::CEnvRegistrationScope scope = registrar.Scope(IEntity::GetEntityScopeGUID());
	{
		Schematyc::CEnvRegistrationScope componentScope = scope.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CServerComponent));
	}
}

CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterServerComponent);

//static CServerComponent* sPServerComponent = nullptr;

CServerComponent::CServerComponent()
{
	//sPServerComponent = this;
}

void CServerComponent::Initialize()
{
	//register RMIs
	ServerSpawnBoxRMI::Register(this, eRAT_NoAttach, true, eNRT_ReliableOrdered);
	ServerAttachBoxRMI::Register(this, eRAT_NoAttach, true, eNRT_UnreliableUnordered);
	ServerThrowBoxRMI::Register(this, eRAT_PostAttach, true, eNRT_UnreliableUnordered);
	ServerDrawTrajectoryRMI::Register(this, eRAT_PostAttach, true, eNRT_UnreliableUnordered);
	ServerStopTrajectoryRenderRMI::Register(this, eRAT_PostAttach, true, eNRT_UnreliableUnordered);
	//make a reference to this ServerComponent on the GamePlugin
	CGamePlugin::GetInstance()->m_pServer = this;
}

//CServerComponent * CServerComponent::GetServerComponent()
//{
//	return sPServerComponent;
//}

bool CServerComponent::NetSerialize(TSerialize ser, EEntityAspects aspect, uint8 profile, int flags)
{
	return true;
}

void CServerComponent::NetReplicateSerialize(TSerialize ser)
{
}

void CServerComponent::SpawnOnServer(Vec3 spawnPos)
{
	if (!gEnv->bServer)
	{
		ServerSpawnBoxRMI::InvokeOnServer(this, SBoxParams{ spawnPos });
	}
}

bool CServerComponent::RemoteSpawnOnServer(SBoxParams && p, INetChannel * pNetChannel)
{
	SEntitySpawnParams sp;
	sp.vPosition = p.spawnPos;
	sp.qRotation = IDENTITY;
	sp.vScale = Vec3(1.f);
	sp.sName = "Box Entity";

	if(IEntity* pBoxEntity = gEnv->pEntitySystem->SpawnEntity(sp))
	{
		pBoxEntity->CreateComponent<CBoxComponent>();
		pBoxEntity->GetNetEntity()->BindToNetwork();
	}

	return true;
}

void CServerComponent::AttachBoxOnServer(Vec3 attachmentPos)
{
	if (!gEnv->bServer)
	{
		ServerAttachBoxRMI::InvokeOnServer(this, SAttachmentProperties{ attachmentPos }, m_pBoxV.back()->GetEntityId());
	}
}

bool CServerComponent::RemoteAttachBoxToPlayer(SAttachmentProperties && p, INetChannel * pNetChannel)
{
	if (gEnv->bServer)
	{ 
		if (IEntity* pPlayerEnt = gEnv->pEntitySystem->FindEntityByName("Player0"))
		{
			if (CPlayerComponent* pPlayer = pPlayerEnt->GetComponent<CPlayerComponent>())
			{
				m_pBoxV.back()->AttachBoxToPlayer(p.attachPos, pPlayerEnt);
			}
		}

		ServerAttachBoxRMI::InvokeOnAllClients(this, SAttachmentProperties{p.attachPos});
	}
	else
	{
		if (IEntity* pPlayerEnt = gEnv->pEntitySystem->FindEntityByName("Player0"))
		{
			if (CPlayerComponent* pPlayer = pPlayerEnt->GetComponent<CPlayerComponent>())
			{
				m_pBoxV.back()->AttachBoxToPlayer(p.attachPos, pPlayerEnt);
			}
		}
	}

	return true;
}

void CServerComponent::ThrowBoxOnServer(Vec3 dirXForce)
{
	if (!gEnv->bServer)
	{
		ServerThrowBoxRMI::InvokeOnServer(this, SThrowBox{ dirXForce });
	}
}

bool CServerComponent::RemoteThrowPlayerBox(SThrowBox&& tb, INetChannel * pNetChannel)
{
	if (gEnv->bServer)
	{	
		//gEnv->pGameFramework->GetNetContext()->DelegateAuthority(m_pBoxV.back()->GetEntityId(), pNetChannel);
		ServerThrowBoxRMI::InvokeOnAllClients(this, SThrowBox{ tb.dirXForce});
		m_pBoxV.back()->ThrowBox(tb.dirXForce);
	}
	else
	{
		//hide the synced client box
		m_pBoxV.back()->GetEntity()->Hide(true);
		
		//spawn and throw a box on the client to visualize a smooth box throwing
		SEntitySpawnParams sp;
		sp.vPosition = m_pBoxV.back()->GetEntity()->GetWorldPos();
		sp.qRotation = m_pBoxV.back()->GetEntity()->GetWorldRotation();
		sp.vScale = Vec3(1.f);
		sp.sName = "Client Box Entity";

		if (IEntity* pCBoxEntity = gEnv->pEntitySystem->SpawnEntity(sp))
		{
			CFakeBoxComponent* fakeBox = pCBoxEntity->CreateComponent<CFakeBoxComponent>();
			fakeBox->ThrowBox(tb.dirXForce);
		}

		//also throw the hidden synced client box
		m_pBoxV.back()->ThrowBox(tb.dirXForce);

		//TODO:
		/*if (m_pBoxV.size() < 5)
		{
			m_pBoxV.back()->GetEntity()->SetPos("insert correct spawn position");
		}*/
	}

	return true;
}

void CServerComponent::DrawTrajectoryOnClients(Vec3 dirXForce)
{
	if (!gEnv->bServer)
	{
		ServerDrawTrajectoryRMI::InvokeOnServer(this, SThrowBox{ dirXForce });
	}
}

bool CServerComponent::RemoteDrawTrajectory(SThrowBox && tb, INetChannel * pNetChannel)
{
	if (gEnv->bServer)
	{
		ServerDrawTrajectoryRMI::InvokeOnAllClients(this, SThrowBox{ tb.dirXForce });
	}
	else
	{
		for (const auto player : m_pPlayerV)
		{
			player->m_allowDrawLine = true;
			player->m_trajectoryPoints = m_pBoxV.back()->GetPositionPoints(tb.dirXForce);
		}

		//CryLogAlways("size of vector is %i", vPoints.size());

		/*if (vPoints.size() > 0)
		{
			gEnv->pRenderer->GetIRenderAuxGeom()->DrawLines(&vPoints[0], vPoints.size(), Col_BlueViolet);
		}*/
	}

	return true;
}

void CServerComponent::StopTrajectoryRenderingOnClients()
{
	if (!gEnv->bServer)
	{
		ServerStopTrajectoryRenderRMI::InvokeOnServer(this, SEmpty{});
	}
}

bool CServerComponent::RemoteStopTrajectoryRender(SEmpty && p, INetChannel * pNetChannel)
{
	if (gEnv->bServer)
	{
		ServerStopTrajectoryRenderRMI::InvokeOnAllClients(this, SEmpty{});
	}
	else
	{
		for (const auto player : m_pPlayerV)
		{
			player->m_allowDrawLine = false;
		}
	}

	return true;
}

<CharacterDefinition CryXmlVersion="2">
 <Model File="Objects/Vehicle/truck/cargo_truck.cga"/>
 <AttachmentList>
  <Attachment Type="CA_BONE" AName="wheel1" RelRotation="1,0,0,0" RelPosition="0,0,0" BoneName="wheel1" Flags="0"/>
  <Attachment Type="CA_BONE" AName="wheel2" RelRotation="1,0,0,0" RelPosition="-0.080242366,0,0" BoneName="wheel2" Flags="0"/>
  <Attachment Type="CA_BONE" AName="wheel3" RelRotation="1,0,0,0" RelPosition="0.074145496,0,0" BoneName="wheel3" Flags="0"/>
  <Attachment Type="CA_BONE" AName="driver" RelRotation="1,0,0,0" RelPosition="-0.36873129,2.439054,0.44830328" BoneName="truck" Flags="0"/>
  <Attachment Type="CA_BONE" AName="box1" RelRotation="1,0,0,0" RelPosition="0,0.70012438,0.28270906" BoneName="truck" Flags="0"/>
  <Attachment Type="CA_BONE" AName="box" RelRotation="1,0,0,0" RelPosition="-0.0093273744,-0.18467009,0.31154889" BoneName="truck" Flags="0"/>
 </AttachmentList>
</CharacterDefinition>

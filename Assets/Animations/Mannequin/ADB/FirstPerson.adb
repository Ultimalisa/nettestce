<AnimDB FragDef="Animations/Mannequin/ADB/PlayerFragmentIds.xml" TagDef="Animations/Mannequin/ADB/PlayerTags.xml">
 <FragmentList>
  <Idle>
  <Fragment BlendOutDuration="0.2" Tags="Walk">
    <AnimLayer>
     <Blend ExitTime="0" StartTime="0" Duration="0.2"/>
     <Animation name="walk_fwd" flags="Loop"/>
    </AnimLayer>
   </Fragment>
  </Idle>
 </FragmentList>
</AnimDB>
